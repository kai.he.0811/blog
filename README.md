## 目录

### Python

* [Flask](Python/Flask.md) ：记录了在使用 Flask 框架中遇到的一些问题
* [Quart](Python/Quart.md) ：记录了在使用 Quart 框架中遇到的一些小问题

### Java

* [kafka 基础概念](Java/Kafka/Kafka基础概念.md)

### 其他

* 代码整洁之道 ：读书笔记

  * [第一章：整洁的代码](代码整洁之道/第一章.md)

    > 代码是写给人看的，只是恰好能让机器运行。
    >
    > Programs must be written for people to read and only incidentally for machines to execute

    > 开发者应该对代码的整洁负责，因为这是我们的责任

  * [第二章：有意义的命名](代码整洁之道/第二章.md)

    > 取好名字最难的地方在于需要良好的描述技巧和共有的文化背景
    
  * [第三章：函数](代码整洁之道/第三章.md)

    > 函数应该只做一件事。做好这一件事。只做一件事
    
  * [第四章：注释](代码整洁之道/第四章.md)
  
    > 真实只在一处地方有：代码。只有代码能忠实地告诉你它做的事。那是唯一真正准确的信息来源
  
  * [第五章：格式](代码整洁之道/第五章.md)
  
    > 代码格式关乎沟通，而沟通是专业开发者的头等大事
    
  * [第六章：对象和数据结构](代码整洁之道/第六章.md)
  
    > 要以最好的方式呈现某个对象包含的数据，需要做严肃的思考。傻乐着乱加乱加取值器和赋值器，是最坏的选择。
    
  * [第七章：对象和数据结构](代码整洁之道/第七章.md)
  
    > 错误处理不过是编程时必须要做的事情之一
  
    > 如果将错误处理隔离看待，独立于主要的逻辑之外，就能写出健壮而整洁的代码。
  
